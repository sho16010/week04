package mshort.week04;

public class UsedCar {

    private String make;
    private String model;
    private int year;
    private String color;
    private int milage;

    public void setMake(String inMake) {
        this.make = inMake;
    }
    public String getMake() {
        return make;
    }

    public void setModel(String inModel) {
        this.model = inModel;
    }
    public String getModel() {
        return model;
    }

    public void setYear(int inYear) {
        this.year = inYear;
    }
    public int getYear() {
        return year;
    }

    public void setColor(String inColor) {
        this.color = inColor;
    }
    public String getColor() {
        return color;
    }

    public void setMilage(int inMilage) {
        this.milage = inMilage;
    }
    public int getMilage() {
        return milage;
    }

    public String toString() {
        return "Year: " + year + ", Make: " + make + " , Model: " + model + " , Color: " + color + ", Milage: " + milage;
    }

}
