package mshort.week04;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class HandleJsonConversion {

    public static UsedCar jsonToUsedCar(String inJsonString) {
        UsedCar myUsedCar = null;
        ObjectMapper myMapper = new ObjectMapper();

        try {
           myUsedCar = myMapper.readValue(inJsonString, UsedCar.class);
        } catch (JsonProcessingException x) {
            System.out.println("JSON Processing Exception Encountered: " + x.getMessage());
        } catch (Exception e) {
            System.out.println("Exception Encountered: " + e.getMessage());
        }

        return myUsedCar;
    }

    public static String usedCarToJson(UsedCar inUsedCar) {
        String result = "";
        ObjectMapper myMapper = new ObjectMapper();

        try {
            result = myMapper.writeValueAsString(inUsedCar);
        } catch (JsonProcessingException x) {
            System.out.println("JSON Processing Exception Encountered: " + x.getMessage());
        } catch (Exception e) {
            System.out.println("Exception Encountered: " + e.getMessage());
        }

        return result;
    }

}
