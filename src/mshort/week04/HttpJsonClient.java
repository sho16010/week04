package mshort.week04;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.lang.StringBuilder;

public class HttpJsonClient {

    public static String getWebContent(String address) {
        String result = "";

        try {
            URL myUrl = new URL(address);
            HttpURLConnection myConnection = (HttpURLConnection) myUrl.openConnection();
            BufferedReader myReader = new BufferedReader(new InputStreamReader(myConnection.getInputStream()));
            /* Using StringBuilder as taught in examples, although we are only sending back one line. If
               that were to ever change, then this would continue to work and not need to be altered. This is
               safer and a better practice for future proofing.
             */
            StringBuilder myStringBuilder = new StringBuilder();
            String currentLine = null;
            while ((currentLine = myReader.readLine()) != null) {
                myStringBuilder.append(currentLine + '\n');
            }
            result = myStringBuilder.toString();
        } catch (Exception x) {
            System.out.println("Exception Caught in getWebContent: " + x.getMessage() + "[" + x.toString() + "]");
        }

        return result;
    }


    public static void main(String[] args) {
        UsedCar ourUsedCar = null;
        String inData = getWebContent("http://localhost:8080");
        if (inData != null && inData.length() > 0) {
            ourUsedCar = HandleJsonConversion.jsonToUsedCar(inData);
            System.out.println("HttpJsonClient: Contents of UsedCar object:\n" + ourUsedCar.toString());
        } else {
            System.out.println("Error: getWebContent did not return any data!");
        }
    }
}
