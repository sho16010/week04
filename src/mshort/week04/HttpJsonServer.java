package mshort.week04;

import java.io.*;
import java.net.InetSocketAddress;
import com.sun.net.httpserver.*;

public class HttpJsonServer {

    private UsedCar serversUsedCar = new UsedCar();

    public static void main(String[] args) {
        HttpJsonServer myHttpServer = new HttpJsonServer();
        try {
            myHttpServer.startServer();
        } catch (Exception e){
            System.out.println("Exception Encountered in main: " + e.getMessage());
        }
    }

    private void startServer() throws Exception {
        HttpServer myServer = HttpServer.create(new InetSocketAddress(8080), 0);
        setServersUsedCar();
        myServer.createContext("/", new MyHttpHandler());
        myServer.setExecutor(null);
        myServer.start();
    }

    /* A Quick Default that can be called to fill a the UsedCar utilized by the server */
    public void setServersUsedCar() {
        serversUsedCar.setYear(1969);
        serversUsedCar.setColor("Orange");
        serversUsedCar.setMake("Dodge");
        serversUsedCar.setModel("Charger");
        serversUsedCar.setMilage(892402);
    }

    public void setServersUsedCar(int inYear, String inColor, String inMake, String inModel, int inMilage) {
        serversUsedCar.setYear(inYear);
        serversUsedCar.setColor(inColor);
        serversUsedCar.setMake(inMake);
        serversUsedCar.setModel(inModel);
        serversUsedCar.setMilage(inMilage);
    }

    private UsedCar getServersUsedCar() {
        return this.serversUsedCar;
    }

    class MyHttpHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange he) throws IOException {
            String serverResponse = HandleJsonConversion.usedCarToJson(getServersUsedCar());
            he.sendResponseHeaders(200, serverResponse.length());
            OutputStream myOutputStream = he.getResponseBody();
            myOutputStream.write(serverResponse.getBytes());
            myOutputStream.flush();
            myOutputStream.close();
            System.out.println("HttpJsonServer: Handled incoming HTTP Connection, sent following info to client:\n" + serverResponse);
        }
    }
}

